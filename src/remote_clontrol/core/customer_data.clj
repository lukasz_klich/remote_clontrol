(ns remote-clontrol.core.customer-data
  (:require [clj-http.client :as client]
            [environ.core :refer [env]]))

(def customer-data (env :customer-data))

(defn- profile-url [country customer]
  (format "%s/%s/customers/%s/profile" customer-data country customer))

(defn- customer-profile [country customer]
  (let [body (:body (client/get (profile-url country customer) {:as :json}))]
    (select-keys body [:customerId :defaultSmartCardId :boxes])))

(defn- smart-card-id [profile smartcard]
  (if (= smartcard "default") (:defaultSmartCardId profile) smartcard))

(defn- box [profile smartcard]
  (first (filter #(= (:smartCardId %) smartcard) (:boxes profile))))

(defn box-info [country customer smartcard]
  (let [profile (customer-profile country customer)
        smartcard (smart-card-id profile smartcard)
        stb (box profile smartcard)]
    (conj (select-keys stb [:isRecordingCapable :isTuningCapable :type])
          {:customerId customer :smartCardId smartcard :country country})))
