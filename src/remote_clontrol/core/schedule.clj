(ns remote-clontrol.core.schedule
  (:require [clj-http.client :as client]
            [environ.core :refer [env]]))

(def schedule (env :schedule))

(defn- broadcasts-url [country]
  (format "%s/data/%s/broadcasts" schedule country))

(defn broadcast-info [country {crid :crid imi :imi}]
  (let [query-params {"limit" 30
                      "fields" "start,video.title"
                      "imi" imi}
        response (client/get (broadcasts-url "PL") {:as :json
                                                    :query-params query-params})

        info (get-in response [:body :data 0])]
    info))

(broadcast-info "PL" {:crid "crid://bds.tv//102804939" :imi "00100000013A5B8A"})
