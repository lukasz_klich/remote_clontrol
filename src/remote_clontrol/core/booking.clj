(ns remote-clontrol.core.booking
  (:require [clojure.tools.logging :as log]
            [remote-clontrol.core.traxis :as traxis]
            [remote-clontrol.core.mds :as mds]))

(defn stb-type [{type :type} _]
  type)

(defmulti send-booking stb-type)

(defmethod send-booking "DVR" [box-info crid-imi]
  (mds/send-dvr box-info crid-imi))

(defmethod send-booking "HORIZON_GATEWAY" [box-info crid-imi]
  (mds/send-hzn box-info))

(defmethod send-booking "DAWN_GATEWAY" [box-info crid-imi]
  (traxis/create (get-in box-info [:customerId]) crid-imi))
