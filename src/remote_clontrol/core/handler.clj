(ns remote-clontrol.core.handler
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [cats.core :as m]
            [ring.middleware.defaults :refer [wrap-defaults api-defaults]]
            [ring.middleware.json :refer [wrap-json-response wrap-json-body]]
            [ring.util.response :refer [response]]
            [metrics.ring.expose :refer [expose-metrics-as-json]]
            [metrics.ring.instrument :refer [instrument]]
            [remote-clontrol.core.booking :as booking]
            [remote-clontrol.core.customer-data :as customer-data]
            [clojure.data.json :as json]
            [clj-time.core :as t]
            [clj-time.format :as f]
            [clojure.tools.logging :as log]))

(defn post-recordings [country customer request]
  (let [crid-imi (select-keys (:body request) [:crid :imi])
        box-info (customer-data/box-info country customer "default")]
    (m/mlet [id (booking/send-booking box-info crid-imi)]
            (m/return (response (merge crid-imi
                                       (select-keys box-info [:smartCardId])
                                       {:tx_id id
                                        :createdAt (f/unparse (f/formatters :date-time-no-ms) (t/now))
                                        }))))))

(m/extract (post-recordings "PL" "3873374" {:body {:crid "crid://bds.tv/199091013"
                                                   :imi "00100000013A9AF7"}}))

(defroutes app-routes
  (POST "/:country/:customer/recordings" [country customer :as request]
    (m/extract (post-recordings country customer request))
    ;; (booking/send-booking
    ;;  {:country (-> country clojure.string/upper-case keyword)
    ;;   :customer customer
    ;;   :stb-type (:stb request)})
    )

  (route/not-found "Not Found"))

(defn init []
  (log/info "
Simple service for remote recordings in LGI.

The authors wish you happy hacking. Wax on, wax off
")

  (def app
    (->
     (routes app-routes)
     (wrap-json-body {:keywords? true})
     (wrap-json-response)
     (wrap-defaults api-defaults)
     (expose-metrics-as-json))))
