(ns remote-clontrol.core.stb-type
  (:require [remote-clontrol.core.customer-data :as customer-data]
            [clojure.tools.logging :as log]))

(defn wrap-box-info
  [handler]
  (fn [request]
    (log/info request)
    (if-let [stb (-> request :params :stb)]
      (handler (conj request {:box-info (keyword stb)}))
      (handler request))))
