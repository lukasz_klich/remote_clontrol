(ns remote-clontrol.core.traxis
  (:require [cats.core :as m]
            [cats.monad.exception :as exc]
            [clj-http.client :as client]
            [clojure.data.xml :as xml]
            [clojure.tools.logging :as log]
            [environ.core :refer [env]]
            [remote-clontrol.core.traxis-requests :as requests]))

(def traxis (env :traxis))

(defn- qname [type]
  (xml/qname "urn:eventis:traxisweb:1.0" type))

(defn- element-type? [type element]
  (= (:tag element) (qname type)))

(defn- recording? [element]
  (element-type? "Recording" element))

(defn- created-id [response]
  (let [recordings (->> (xml/parse-str (:body response))
                       xml-seq
                       (filter recording?))]
    (get-in (first recordings) [:attrs :id])))

(defn- recorded-id [response event-id]
  (let [recordings (->> (xml/parse-str (:body response))
                       xml-seq
                       (filter recording?)
                       (filter #(some (fn [tag] (= (first (:content tag)) event-id)) (:content %))))]
    (get-in (first recordings) [:attrs :id])))

(defn- extract-state-and-id [recording]
  (let [children (:content recording)
        id (get-in recording [:attrs :id])
        state (first  (:content (first (filter #(element-type? "State" %) children))))]
    {:id id
     :state state}))

(defn- call [body]
  (exc/try-on (client/post traxis {:body body})))

(defn recorded [customer-id crid-imi]
  (m/mlet [response (call (requests/get-recordings-request customer-id))]
          (m/return (recorded-id response (requests/unescaped-resource-id crid-imi)))))

(defn- already-recorded-recovery [customer-id crid-imi]
  (fn [e]
    (let [data (ex-data e)
          body (:body data)
          status (:status data)]
      (if (and (.contains body "is already recorded")
               (= status 400))
        (recorded customer-id crid-imi)
        (exc/failure e)))))

(defn- recover [recovery exception]
  (if (exc/success? exception)
    exception
    (recovery (m/extract exception))))

(defn- failing-create [customer-id crid-imi]
  (m/mlet [response (call (requests/create-recording customer-id crid-imi))]
          (m/return (created-id response))))

(defn create [customer-id crid-imi]
  (let [recovery (partial recover (already-recorded-recovery customer-id crid-imi))]
    (m/mlet [id (recovery (failing-create customer-id crid-imi))]
            (m/return id))))
