(ns remote-clontrol.core.mds
  (:require [remote-clontrol.core.schedule :as schedule]))

(defn send-dvr [box-info crid-imi]
  (let [broadcast-info (schedule/broadcast-info (:country box-info) crid-imi)]
    (str "sending to mds dvr data " box-info " " broadcast-info)))

(defn send-hzn [data]
  (str "sending to mds data " data))
