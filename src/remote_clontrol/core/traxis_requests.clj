(ns remote-clontrol.core.traxis-requests
  (:require [clojure.data.xml :as xml]))

(defn- ident [customer-id]
  (xml/element :Identity {}
               (xml/element :CustomerId {} customer-id)))

(defn- request [id query]
  (xml/emit-str (xml/element :Request {:xmlns "urn:eventis:traxisweb:1.0"}
                             id
                             query)))

(defn unescaped-resource-id [crid-imi]
  (let [{crid :crid imi :imi} crid-imi]
    (format "%s,imi:%s" crid imi)))

(defn resource-id [crid-imi]
  (clojure.string/replace (unescaped-resource-id crid-imi) "/" "~~2F"))

(defn- record-action-query [crid-imi]
  (let [arguments (xml/element :Arguments {}
                               (xml/element :Argument {:name "Location"}
                                            "Local"))]
    (xml/element :ActionQuery {:resourceType "Event"
                               :resourceId (resource-id crid-imi)
                               :actionName "Record"}
                 arguments)))

(defn create-recording [customer-id crid-imi]
  (let [query (record-action-query crid-imi)]
    (request (ident customer-id)
             (record-action-query crid-imi))))

(defn get-recordings-request [customer-id]
  (let [ident (ident customer-id)
        query (xml/element :RootRelationQuery {:relationName "Recordings"}
                           (xml/element :Options {}
                                        (xml/element :Option {:type "Props"} "State,EventId")))]
    (request ident query)))
